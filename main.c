#include <stdio.h>
#include <stdint.h>

#define printAndContinue(x) { printf(x); continue; }

int main() {
	for (int_fast64_t i = 1; i < 64; i++) {
		if (!((i % 3) || (i % 5))) {
			printAndContinue("Fizz Buzz, ");
		}
		if (!(i % 3)) {
			printAndContinue("Fizz, ");
		}
		if (!(i % 5)) {
			printAndContinue("Buzz, ");
		}
		printf("%d, ", i);
	}
}
